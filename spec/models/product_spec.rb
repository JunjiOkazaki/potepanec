require 'rails_helper'

RSpec.describe "Product_model", type: :model do
  let(:category) { create(:taxonomy, name: "Category") }
  let(:same_taxon1) { create(:taxon, name: "Same Taxon1", taxonomy: category, parent: category.root) }
  let(:same_taxon2) { create(:taxon, name: "Same Taxon2", taxonomy: category, parent: category.root) }
  let(:other_taxon) { create(:taxon, name: "Other Taxon", taxonomy: category, parent: category.root) }
  let(:product) { create(:product, taxons: [same_taxon1, same_taxon2], name: "Product") }

  describe "related_products" do
    let(:same_taxon_product) { create(:product, taxons: [same_taxon1, same_taxon2], name: "Same Taxon Product") }
    let(:other_taxon_product) { create(:product, taxons: [other_taxon], name: "Other Taxon Product") }
    let(:related_products) do
      product.related_products
    end

    it "related_products should have same taxon product" do
      expect(related_products).to include same_taxon_product
    end

    it "related_products should not have othre taxon product and original product" do
      expect(related_products).not_to include other_taxon_product
      expect(related_products).not_to include product
    end

    it "related_products should not have duplication" do
      expect(related_products).to eq related_products.uniq
    end
  end
end
