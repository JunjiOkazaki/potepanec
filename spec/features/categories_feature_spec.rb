require 'rails_helper'

RSpec.feature "Categories_feature", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let!(:bag) { create(:taxon, name: "bags", taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:tote) { create(:product, name: "Ruby on Rails Tote", price: "23.45", taxons: [bag]) }

  scenario "should be able to visit other category" do
    visit potepan_category_path(taxonomy.root.id)
    expect(page).to have_link bag.name, href: potepan_category_path(bag.id)
    find('.collapse', text: 'Category').click
    find('#content-link', text: 'bags').click
    expect(current_path).to eq potepan_category_path(bag.id)
  end

  scenario "side-bar's taxon count should equal displayed products count" do
    visit potepan_category_path(taxonomy.root.id)
    expect(page).to have_selector('.productBox', count: bag.products.count)
  end

  scenario "should be able to visit products" do
    visit potepan_category_path(taxonomy.root.id)
    expect(page).to have_link tote.name, href: potepan_product_path(tote.id)
    find('#product-link', text: 'Ruby on Rails Tote').click
    expect(current_path).to eq potepan_product_path(tote.id)
  end
end
