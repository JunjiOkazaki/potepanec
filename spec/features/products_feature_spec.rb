require 'rails_helper'

RSpec.feature "Products_feature", type: :feature do
  let(:category) { create(:taxonomy, name: "Category") }
  let(:bag) { create(:taxon, name: "Bags", taxonomy: category, parent: category.root) }
  let(:mug) { create(:taxon, name: "Mugs", taxonomy: category, parent: category.root) }
  let(:product) { create(:product, name: "Tote", price: "15.99", taxons: [bag]) }
  let!(:other_product) { create(:product, name: "Mini mug", price: "45.99", taxons: [mug]) }
  let!(:related_products) { create_list(:product, 5, taxons: [bag], name: "Mini bag", price: "9.99") }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "View show page" do
    expect(page).to have_current_path potepan_product_path(product.id)
    expect(page).to have_title "#{product.name} - BIGBAG Store"
    within('.page-title') do
      expect(page).to have_selector "h2", text: product.name
    end
    within('.media-body') do
      expect(page).to have_selector "h2", text: product.name
      expect(page).to have_selector "h3", text: product.display_price
      expect(page).to have_selector "p", text: product.description
    end
    expect(page).to have_link 'Home', href: potepan_index_path
    expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(product.taxons.first.id)
  end

  scenario "should be able to show 4 related products" do
    expect(page).to have_selector '.productBox', count: 4
    within first('.productCaption') do
      expect(page).not_to have_selector "h5", text: product.name
      expect(page).not_to have_selector "h3", text: product.display_price
      expect(page).not_to have_selector "h5", text: other_product.name
      expect(page).not_to have_selector "h3", text: other_product.display_price
      related_products.all? do |related_product|
        expect(page).to have_selector "h5", text: related_product.name
        expect(page).to have_selector "h3", text: related_product.display_price
      end
    end
  end

  scenario "should be able to visit related product" do
    related_product = related_products[0]
    within first('.productBox') do
      find('#related-link', text: 'Mini bag').click
    end
    expect(current_path).to eq potepan_product_path(related_product.id)
  end
end
