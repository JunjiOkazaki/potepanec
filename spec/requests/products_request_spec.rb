require 'rails_helper'

RSpec.describe "Products_request", type: :request do
  let(:category) { create(:taxonomy, name: "Category") }
  let(:taxon) { create(:taxon, name: "Bags", taxonomy: category, parent: category.root) }
  let(:product) { create(:product, taxons: [taxon], name: "Tote", price: "15.99", description: "Good") }
  let!(:related_products) { create_list(:product, 3, taxons: [taxon], name: "Mini Bag", price: "13.56", description: "Great") }
  let!(:fourth_related_product) { create(:product, taxons: [taxon], name: "L Bag", price: "20.99", description: "Marvelous") }
  let!(:fifth_related_product) { create(:product, taxons: [taxon], name: "XL Bag", price: "25.99", description: "Excellent") }

  describe "GET #show" do
    before do
      get potepan_product_url(product.id)
    end

    it "responds successfully and 200 response" do
      expect(response).to have_http_status(200)
    end

    it "show correct View" do
      expect(response).to render_template :show
    end

    it "show templete products display" do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
      expect(response.body).to include product.description
    end

    it "show templete 4th related_product display" do
      expect(response.body).to include fourth_related_product.name
      expect(response.body).to include fourth_related_product.display_price.to_s
      expect(response.body).not_to include fourth_related_product.description
    end

    it "does not show templete 5th related_product display" do
      expect(response.body).not_to include fifth_related_product.name
      expect(response.body).not_to include fifth_related_product.display_price.to_s
      expect(response.body).not_to include fifth_related_product.description
    end
  end
end
