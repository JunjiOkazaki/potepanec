require 'rails_helper'

RSpec.describe "Categories_request", type: :request do
  describe "GET categories#show" do
    let(:category) { create(:taxonomy, name: "Category") }
    let(:bag) { create(:taxon, name: "Bag", taxonomy: category) }
    let!(:rails_bag) { create(:product, taxons: [bag], name: "Ruby on Rails Bag") }

    before do
      get potepan_category_path(bag.id)
    end

    it "responds successfully and 200 response" do
      expect(response).to have_http_status(200)
    end

    it "show correct View" do
      expect(response).to render_template :show
    end

    it "show templete categories display" do
      expect(response.body).to include category.name
      expect(response.body).to include bag.name
      expect(response.body).to include rails_bag.name
    end
  end
end
